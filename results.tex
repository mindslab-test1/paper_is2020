\section{Results}

\subsection{Many-to-many conversion}

We compare our system with Blow \cite{blow},
which is the only literature to date on many-to-many voice conversion
with all speakers of VCTK.
As presented in Table \ref{tab:evaluation},
our system shows significantly better results on
both MOS and DMOS than Blow,
even when only the Cotatron features are used without residual features.
Incorporating the residual encoder on our system has further enhanced the MOS.
It should, however, be noted that
the objective results on speaker similarity (SCA)
are contradicting that from the subjective results (DMOS).
Future work should, therefore, revisit and establish
objective speaker similarity metrics for voice conversion systems.


\begin{table}[h]
	\caption{
		Results of many-to-many voice conversion.
	}
	\label{tab:evaluation}
	\centering
	\begin{tabularx}{\linewidth}{lXccc}
		\toprule
		\multicolumn{2}{l}{\textbf{Approach}} & \textbf{MOS} & \textbf{DMOS} & \textbf{SCA} \\
		\midrule
		\multicolumn{2}{l}{Source as target}
		& $ 4.28 \pm 0.11 $ & $ 1.71 \pm 0.22 $  & \phantom{0}0.9\% \\
		\multicolumn{2}{l}{Target as target}
		& $ 4.28 \pm 0.11 $ & $ 4.78 \pm 0.08 $ & 99.4\% \\
		\midrule
		\multicolumn{2}{l}{Blow}
		& $ 2.41 \pm 0.14 $ & $ 1.95 \pm 0.16 $ & \textbf{86.8\%}  \\
		\multicolumn{2}{l}{\textbf{Cotatron} (ours)} &  &  & \\
		\hspace{-0.2cm} & w/o residual
		& $ 3.18 \pm 0.14 $ & $ \mathbf{4.06 \pm 0.17} $ & 73.3\% \\
		\hspace{-0.2cm} & \textbf{full model}
		& $ \mathbf{3.41 \pm 0.14} $ & $ 3.89 \pm 0.18 $ & 78.5\%   \\
		\bottomrule
	\end{tabularx}
\end{table}


\subsection{Any-to-many conversion and the use of ASR}

Considering the technical demands of the real-world applications,
we further explore the generalization power of our voice conversion system.
First, we consider any-to-many setting -- \ie,
converting arbitrary speakers' speech to that of speakers that are seen during training.
Next, we inspect the reliability of using ASR transcription,
which enables a fully automatic pipeline of our system without manual transcription.
For any-to-many conversion experiment,
we randomly sample speeches from LibriTTS \emph{test-clean} split
and convert them into speakers of VCTK.
For ASR, wav2letter++ \cite{zeghidour2018fully, wav2letter++} is used.

In Table \ref{tab:asr_unseen},
we present the MOS, SCA, and VDE for all possible cases of input.
First,
all of the MOS results are much better than the previous method in Table \ref{tab:evaluation},
though the scores from any-to-many setting are slightly lower than that of many-to-many setting.
Next, the differences of SCA across the cases are negligible,
and the values of VDE are minimal when considering the accuracy of the VAD module.
These results suggest that the conversion quality is rather unaffected by using
(1) source speech from speakers that are unseen during training,
and/or
(2) automated transcription from ASR.
Besides, it is surprising to observe that
the word errors of automated transcription
do not damage the performance;
this would seem to suggest that
most of the transcription errors originate from their homophones,
\eg, \textit{site} is often wrongly transcribed as \textit{sight}.

\begin{table}[h]
	\caption{
		Results of any-to-many conversion and using ASR transcription.
		The values are expected to be similar across the rows.
	}
	\label{tab:asr_unseen}
	\centering
	\begin{tabularx}{\linewidth}{Xccc}
		\toprule
		\textbf{Input Transcription} & \textbf{MOS} & \textbf{SCA} &  \textbf{VDE}  \\
		\midrule
		\multicolumn{3}{l}{\textit{VCTK test $ \rightarrow $ VCTK test (many-to-many)}} \\
		1-a. ground truth
		& $ 3.41 \pm 0.14 $ & 78.5\% & 2.98\% \\
		1-b. ASR (WER 12.6\%)
		& $ 3.44 \pm 0.12 $ & 77.8\% & 3.03\% \\
		\midrule
		\multicolumn{3}{l}{\textit{LibriTTS test-clean $ \rightarrow $ VCTK test (any-to-many)}} \\
		2-a. ground truth
		& $ 2.84 \pm 0.14 $ & 73.6\% & 11.9\% \\
		2-b. ASR (WER 7.0\%)
		& $ 2.83 \pm 0.15 $ & 71.7\% & 11.7\% \\
		 \bottomrule
	\end{tabularx}
\end{table}

\subsection{Degree of disentanglement} \label{subsec:disentanglement}
To quantify the degree of speaker disentanglement
of features from Cotatron and the residual encoder,
we additionally train a neural network
for classifying speakers from the VCTK dataset with a given set of features.
In the case of ideal speaker disentanglement,
the SCA will be close to that of random guessing: 0.9\%.
Each classification network is built with 4 layers of 1D CNN and batch normalization,
followed by the temporal max-pooling layer and MLP with dropout.

As shown in Table \ref{tab:disentanglement},
the SCA with Cotatron features and the residual features
are significantly lower than that from the source \mel.
These results indicate that our method effectively disentangles
the speaker's identity from the speech,
while it is noteworthy to mention that the network
was slightly able to guess the speaker using only Cotatron features.

\begin{table}[h]
	\caption{%
		Degree of speaker disentanglement.
	}
	\label{tab:disentanglement}
	\centering
	\begin{tabularx}{\linewidth}{Xcccc}
		\toprule
		\textbf{Input Feature} & Random  & $ L_{s} $ & $ (L_{s}, R_{s}) $ & $ M_{s} $ \\
		\midrule
		\textbf{SCA} & 0.9\% & 35.2\% & 54.0\% & 97.9\% \\
		\bottomrule
	\end{tabularx}
\end{table}
