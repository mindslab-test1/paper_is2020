\section{Approach} \label{sec:model}

\subsection{Speaker-independent linguistic features from TTS} \label{subsec:bmm}

\begin{figure}[t]
	\centering
	\includegraphics[width=\linewidth]{diagram.pdf}
	\caption{
		Cotatron architecture.
		The alignment between \mel and its transcription
		is obtained via a pre-trained multispeaker TTS (Tacotron2)
		and then combined with text encoding
		to extract speaker-independent linguistic features.
		\emph{Spk.} denotes the speaker encoder.
	}
	\label{fig:linguistic}
\end{figure}

Cotatron is guided with a transcription
to extract speaker-independent linguistic features from the speech.
Cotatron's basic architecture is identical to multispeaker Tacotron2 \cite{tacotron2, sv2tts};
it jointly learns to align and predict next mel frame
from the text encoding, previous mel frame, and the speaker representation:
\begin{equation}\label{eq:tacotron2}
	\hat{M}_{1:i}, A_{i} = \mathrm{\underset{tts}{Decoder}}\left(
		\mathrm{\underset{text}{Encoder}}\left(T\right),
		M_{0:i-1}, z^{id}\right),
\end{equation}
where $ T, M, A, z^{id} $ corresponds to text,
log \mel, alignment, and the speaker representation, respectively.

After training, a simple yet effective trick is applied.
An alignment $ A $ between the speech and the transcription is obtained
via feeding all frames of the \mel into Cotatron with teacher-forcing applied.
Then, the speaker-independent linguistic features of the speech are obtained from
a matrix multiplication of
the alignment and text encoding as Fig. \ref{fig:linguistic}:
\begin{equation}\label{eq:bmm}
	L = \mathtt{matmul}\left(A, \mathrm{\underset{text}{Encoder}}(T)\right).
\end{equation}
Per definition, the text encoding contains no speaker information.
Besides, the text-audio alignment $ A $
is a set of scalar coefficients for weighted summation over encoder timesteps of the text encoding.
Hence, we may argue that the Cotatron features $ L $
do not explicitly contain a source speaker's information.
We show the degree of speaker disentanglement at Sec. \ref{subsec:disentanglement}.

Cotatron features are naturally adequate for
synthesizing speech from a large number of speakers;
the features can be interpreted as
context vectors for Tacotron2's attention mechanism,
which are already optimized for multispeaker speech synthesis.
We further expand the coverage of source speakers into arbitrary
by replacing
the embedding table into an encoder for speaker representation $ z^{id} $.
The speaker encoder is composed of 6 layers of 2D CNN,
following the reference encoder architecture from
Skerry-Ryan \etal \cite{e2eprosody}.
Each layer had $ 3\times3 $ kernel, $ 2\times2 $ stride
with 32, 32, 64, 64, 128, 128 channels.
The CNN output is flattened and
passed through a 256-unit GRU
to obtain the fixed-length speaker representation
from the final state.

\subsection{Voice conversion}

\subsubsection{Residual Encoder}
Let's consider a decoder reconstructing the speech from Cotatron features.
Even when the rhythm of the transcription is given via Cotatron features,
other components of the speech may vary.
For example, the intonation may vary
within the speech of the same text with identical rhythm.
It is therefore insufficient for the decoder 
to use only the Cotatron features and the speaker representation.
To fill the gap of information,
we design an encoder to provide decoder a residual feature $ R $.

The residual encoder (Fig. \ref{fig:residual_encoder}) is built with 6 layers of 2D CNN
as the speaker encoder did,
but strides are not applied across time
to preserve the temporal dimension of the \mel.
Each layer had $ 3\times3 $ kernel with $ 2\times1 $ stride
and 32, 32, 64, 64, 128, 128 channels.
If the dimension of the residual features is too wide,
the residual encoder may learn to cheat by encoding the information
that is related to the individual speaker -- \eg, absolute pitch.
We find that a single-channeled output helps
to prevent the residual features from containing
characteristic of the individual speaker,
and is enough to represent residual information of the speech;
this approach was also used by Lian \etal \cite{lian2019towards}.
After projecting to a single channel,
instance normalization \cite{inorm} is applied
to prevent the residual representation from containing speaker-dependent information.
Finally,
the values are smoothed after tanh activation by applying convolution with a Hann function
of window size 21.


\subsubsection{VC Decoder} \label{subsubsec:decoder}

The decoder for voice conversion (Fig. \ref{fig:decoder}) is trained to reconstruct the \mel
from a given pair of information;
the Cotatron features $ L $ 
and the residual feature $ R $ 
are concatenated channel-wise,
and then conditioned with
256-dimension speaker embedding $ y^{id} $ retrieved from a lookup table as:
\begin{equation}\label{eq:decoder}
	M_{s\rightarrow\ast} = \mathrm{\underset{vc}{Decoder}}\left( \mathtt{concat}\left(L_{s}, R_{s}\right), y_{\ast}^{id} \right).
\end{equation}
The asterisk symbol
can be either $ s $ or $ t $,
each representing source/target of the voice conversion.
Thus, $ M_{s\rightarrow s} $ denotes reconstruction,
and $ M_{s\rightarrow t} $ denotes voice conversion from $ s $ to $ t $.

Following the model architecture of GAN-TTS \cite{gantts},
the VC decoder is constructed with
a stack of four \textit{GBlock}s without upsampling.
Each GBlock has 512, 384, 256, 192 channels, respectively.
For speaker conditioning, the embedding of the target speaker $ y^{id} $
is injected via a conditional batch normalization layer \cite{condbn}
within the GBlocks,
after an affine transformation.
We empirically observed that concatenation of speaker embedding leads to worse results.
Neither the hyper-conditioning \cite{hypernet} nor the weight demodulation \cite{stylegan2} did not help.
There might be room for improvement in design choices of decoder architecture,
but we leave it as a future work since it is beyond the scope of this work.


\begin{figure}[t]
	\centering
	\begin{subfigure}[t]{\linewidth}
		\centering
		\includegraphics[width=\linewidth]{overall.pdf}
		\caption{Voice Conversion system with Cotatron.}
		\label{fig:overall}
	\end{subfigure}
	\vspace{0.2cm}
	
	\hspace{-0.4cm}
	\begin{subfigure}[t]{0.49\linewidth}
		\centering
		\includegraphics[height=0.21\textheight]{residual_encoder.pdf}
		\caption{Residual encoder.}
		\label{fig:residual_encoder}
	\end{subfigure}
	\begin{subfigure}[t]{0.49\linewidth}
		\centering
		\includegraphics[height=0.21\textheight]{student.pdf}
		\caption{VC decoder.}
		\label{fig:decoder}
	\end{subfigure}
	\caption{%
		Network architectures.
		$ s, c, k $ denotes the
		stride, number of channels, kernel size
		of convolution layer, respectively.
		Speaker representation $ y^{id} $ conditions the VC decoder
		via conditional batch normalization layer
		within residual blocks.
		Refer to Binkowski \etal \cite{gantts}
		for the detailed architecture of GBlock.
	}
	\label{fig:mel_encoder}
\end{figure}

Note that the VC decoder is only trained to reconstruct the \mel with representations from the identical speaker.
Though it is possible to directly train the conversion in an adversarial manner,
we show the effectiveness of Cotatron on voice conversion
using only reconstruction loss.
