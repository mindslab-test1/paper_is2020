\section{Experimental setup}

\subsection{Dataset}
Our voice conversion system is trained and evaluated with a VCTK dataset \cite{vctk},
which consists of 46 hours of English speech from 108 speakers.
Similar to what Blow had done \cite{blow},
we split the data into train, validation, and test splits
by randomly selecting 80\%, 10\%, 10\% of the data, respectively.
To prevent overlap of transcription between data splits,
the data is split with respect to their transcription,
not the number of files.

To stabilize the training of multispeaker TTS,
we incorporate a subset of LibriTTS \cite{libritts},
which is a dataset specialized in training TTS systems.
Speakers with more than 5 minutes of speech
are chosen from LibriTTS' \textit{train-clean-100} subset.

All audios longer than 10 seconds are not used for training
to allow efficient batching.
The audios are resampled to sampling rate \SI{22.05}{\kilo\hertz}
and then normalized without silence removal.
The statistics of the dataset are shown in Table \ref{tab:dataset}.

\begin{table}[h]
	\caption{%
		Dataset statistics.
		For LibriTTS \emph{train-clean-100} split,
		speakers with less than 5 minutes of speech are removed.
	}
	\label{tab:dataset}
	\begin{tabularx}{\linewidth}{Xcc}
		\toprule
		\textbf{Dataset} & \textbf{\# speakers} & \textbf{Length (h)} \\
		\midrule
		VCTK \cite{vctk} train / val / test & 108 & 34.6 / 4.5 / 4.2 \\
		\midrule
		LibriTTS \cite{libritts} && \\
		\emph{train-clean-100} & 123 & 23.4 \\
		\emph{dev-clean} & \phantom{0}40 & \phantom{0}9.0 \\
		\emph{test-clean} & \phantom{0}39 & \phantom{0}8.6 \\
		\bottomrule
	\end{tabularx}
\end{table}



\subsection{Training}

\subsubsection{Cotatron}
Cotatron is trained with the aforementioned subset of LibriTTS,
which is based on the \emph{train-clean-100} split.
Then, the model is transferred to learn with both LibriTTS and VCTK train split.
To enhance the stability of text-audio alignment learning,
the autoregressive decoder is teacher-forced with a rate of 0.5,
\ie, input mel frame is randomly selected from
either ground truth frame or previously generated frame.
Furthermore, we find it helpful to train extra MLP with dropout
for speaker classification on top of $ z^{id} $ from the speaker encoder,
using cross-entropy loss $ \mathcal{L}_{id} $.
Overall, Cotatron is trained with
the sum of \mel reconstruction loss and speaker classification loss:
\begin{equation}\label{eq:cotatron_loss}
	\mathcal{L}_{\text{cotatron}} =
	\left\| \hat{M}_{s, pre} - M_{s} \right\|_{2}^{2}
	+ \left\| \hat{M}_{s, post} - M_{s} \right\|_{2}^{2}
	+ \mathcal{L}_{id},
\end{equation}
where $ \hat{M}_{s, pre} $ and $ \hat{M}_{s, post} $
denote output before and after
the Cotatron's post-net \cite{tacotron2}, respectively.

Throughout the training process, Adam optimizer \cite{adam} is used
with batch size 64.
The initial learning rate \num{3e-4} is used for the first 25k steps
and then exponentially decayed to \num{1.5e-5} for the next 25k steps.
After the model converges with LibriTTS, we add VCTK
and reuse the learning rate decay scheme.
Weight decay of \num{1e-6} is used for Adam optimizer,
and the gradient is clipped to 1.0 to prevent gradient explosion.

\subsubsection{Mel-Spectrogram Reconstruction}
After the training of Cotatron,
the components for voice conversion system is trained
on top of Cotatron features.
The residual encoder and the VC decoder is jointly trained
with \mel reconstruction loss:
\begin{equation}\label{eq:reconstruction}
	\mathcal{L}_{\text{vc}} = \left\| M_{s\rightarrow s} - M_{s} \right\|_{2}^{2}.
\end{equation}
During the reconstruction training phase,
Cotatron is set to evaluation mode;
all dropout layers are turned off,
and the autoregressive decoder is always teacher-forced
to provide consistent features for VC decoder.
Adam optimizer with constant learning rate \num{3e-4}
is used with weight decay \num{1e-6} and batch size 128.
Gradient clipping is not used here.

\subsection{Conversion}

To convert one voice to another,
we first extract the speaker-independent features, $ L_{s}, R_{s} $,
from the source speech with Cotatron and residual encoder, respectively.
Then, the embedding of the target speaker $ y_{t}^{id} $
is retrieved from the lookup table.
Finally, a pair of speaker-independent features and target speaker embedding
is used to produce a converted \mel, $ M_{s\rightarrow t} $.
The resulting \mel is then inverted into raw audio using MelGAN \cite{melgan},
which is trained with LibriTTS train split and then fine-tuned with the entire VCTK dataset.

\subsection{Implementation details}

For robust alignment stability against length variation,
we apply the Dynamic Convolution Attention (DCA) mechanism \cite{dca}.
The speaker representation is extracted from
the ground-truth \mel with the speaker encoder,
and then repeatedly concatenated with text encoder output
to feed the auto-regressive decoder of Cotatron.
For both Cotatron and the voice conversion system,
the training data is augmented with representation mixing \cite{kastner2019representation},
\ie, graphemes are randomly replaced with phonemes if the word is available in CMUdict \cite{cmudict}.
Both decoders produce 80-bin log \mel,
which is computed from \SI{22.05}{\kilo\hertz} raw audio using
STFT with window size 1024, hop size 256, Hann window,
and a mel filterbank spanning from \SI{70}{\hertz} to \SI{8000}{\hertz}.
The voice conversion systems are implemented with PyTorch \cite{pytorch}
and trained for 10 days with two NVIDIA V100 (32GB) GPU
using data parallelism.

%Since the length of training samples are not identical,
%they are zero-padded to match the batch's max length for GPU efficiency.
%Therefore, the output of every convolutional layer is masked
%to prevent the results of the padded areas from affecting the non-padded area,
%as pointed out by Bi{\'n}kowski \etal \cite{gantts}.
%In addition, we exclude the padded area
%from the statistics calculation for the instance normalization layer
%of the residual encoder.% (Figure \ref{fig:prosody_encoder}).


\subsection{Evaluation metrics}

We validate the effectiveness of our method
with both subjective and objective metrics,
using 100 and 10,000 audio samples per each measurement, respectively.

\vspace{5pt}\noindent\textbf{Mean Opinion Score (MOS).}
To assess the naturalness of converted speech,
we measure the mean opinion score (MOS) on a 5-point scale at Amazon Mechanical Turk (MTurk).
A total of 100 audio samples are generated for each case
with a random pair of source speech and target speaker,
which contains all possible gender combinations.
The audio samples from our method and natural speech are downsampled to rate \SI{16}{\kilo\hertz}
to match the results from Blow \cite{blow}.
Each sample is assigned to 5 human listeners,
and the highest/lowest score is discarded.

%The listeners are required to have Master Qualification,
%and requested to wear headphones
%while listening to each audio sample at least twice.

\vspace{5pt}\noindent\textbf{Degradation Mean Opinion Score (DMOS).}
Another user study is done to assess speaker similarity
between the converted speech and the target speaker's original recording.
The degradation mean opinion score (DMOS) on a 5-point scale
is measured at MTurk with the same settings from the MOS experiment.

\vspace{5pt}\noindent\textbf{Speaker Classification Accuracy (SCA).}
Our system should be able to fool the
speaker classifier as if the converted speech
was spoken from the target speaker.
The speaker classifier is an
MFCC-based single-layer classifier,
which is identical to
the one used with Blow \cite{blow}
for a fair comparison.
The classifier is trained with
108 speakers from the VCTK train split
and achieved 99.4\% top-1 accuracy on the test split.
The MFCC is directly calculated from the log \mel if possible.

\vspace{5pt}\noindent\textbf{Voicing Decision Error (VDE).}
As a proxy metric for
content consistency between source and converted speech,
we measure the rate of voicing decision match between them,
adapting a metric of end-to-end prosody transfer for speech synthesis \cite{e2eprosody}.
The voicing decision is obtained via rVAD \cite{tan2020rvad}
with a VAD threshold value set to 0.7.
